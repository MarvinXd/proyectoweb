/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.mysql.fabric.xmlrpc.base.Value;
import com.utilidades.Persistencia;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;



import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;

import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletResponse;
import javax.swing.ImageIcon;
import modelo.dao.AutorDao;
import modelo.dao.LibroDao;

import modelo.entidad.Autor;
import modelo.entidad.Libro;

import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;


import java.io.File;
import java.io.FileInputStream;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

import modelo.dao.CategoriaDao;
import modelo.entidad.Categoria;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.net.util.Base64;
import org.json.simple.JSONObject;
import sun.misc.BASE64Encoder;


/**
 *
 * @author elcon
 */
@ManagedBean(name ="bkn_Libro")
@RequestScoped
public class LaboratorioDesaUI {
   private String previousPage = null;
   private List<Integer>listaAnios;
   private List<Autor>listaAutores;
    private List<Categoria>listaCategorias;
   private List<Libro>listaLibros;
   private AutorDao autorDao;
   private CategoriaDao categoriaDao;
   private Libro libro; 
   private LibroDao libroDao;
   private  ImageIcon icono;


   private UploadedFile file;  
   private StreamedContent imagem;
   
   private String imagenLibroTemporal;
   private String titul;
    private static int idAux;
    private Persistencia persistencia;
   
   
   
   
@PostConstruct
  public void init(){
  //listaCategorias= new ArrayList();
  libro = new Libro();
  libroDao = new LibroDao();
  listaAnios= new ArrayList();
  listaAutores= new ArrayList();
  listaCategorias= new ArrayList();
  autorDao = new AutorDao();
  categoriaDao = new CategoriaDao();
  persistencia= new Persistencia();

  LlenarListaAnios();
  llenarListaLibros();
  llenarLibros();
 descargarimagenes();
 // titul = libro.getTitulo();

    
  }
 public void refrescar(){
   listaLibros = libroDao.obtenerLibros();  
 }
    public AutorDao getAutorDao() {
        return autorDao;
    }

    public void setAutorDao(AutorDao autorDao) {
        this.autorDao = autorDao;
    }

    public CategoriaDao getCategoriaDao() {
        return categoriaDao;
    }

    public void setCategoriaDao(CategoriaDao categoriaDao) {
        this.categoriaDao = categoriaDao;
    }

    public List<Autor> getListaAutores() {
        return listaAutores;
    }

    public void setListaAutores(List<Autor> listaAutores) {
        this.listaAutores = listaAutores;
    }

    public List<Categoria> getListaCategorias() {
        return listaCategorias;
    }

    public void setListaCategorias(List<Categoria> listaCategorias) {
        this.listaCategorias = listaCategorias;
    }

    public static int getIdAux() {
        return idAux;
    }

    public static void setIdAux(int idAux) {
        LaboratorioDesaUI.idAux = idAux;
    }

    public String getTitul() {
        return titul;
    }

    public void setTitul(String titul) {
        this.titul = titul;
    }

    public String getImagenLibroTemporal() {
        return imagenLibroTemporal;
    }

    public void setImagenLibroTemporal(String imagenLibroTemporal) {
        this.imagenLibroTemporal = imagenLibroTemporal;
    }

    public String getPreviousPage() {
        return previousPage;
    }

    public void setPreviousPage(String previousPage) {
        this.previousPage = previousPage;
    }

    public StreamedContent getImagem() {
        return imagem;
    }

    public void setImagem(StreamedContent imagem) {
        this.imagem = imagem;
    }

    public ImageIcon getIcono() {
        return icono;
    }

    public void setIcono(ImageIcon icono) {
        this.icono = icono;
    }

   

    public List<Libro> getListaLibros() {
        return listaLibros;
    }

    public void setListaLibros(List<Libro> listaLibros) {
        this.listaLibros = listaLibros;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public LibroDao getLibroDao() {
        return libroDao;
    }

    public void setLibroDao(LibroDao libroDao) {
        this.libroDao = libroDao;
    }

   


    public List<Integer> getListaAnios() {
        return listaAnios;
    }

    public void setListaAnios(List<Integer> listaAnios) {
        this.listaAnios = listaAnios;
    }

    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }

  
public Integer obtenerFechaActual(){
  Calendar fecha = new GregorianCalendar();
  int anio = fecha.get(Calendar.YEAR);
 return anio;
       
   }
  public void llenarListaLibros(){
     
      
       try {
           
           listaAutores= persistencia.autores();
           listaCategorias= persistencia.ConsumirListaCategorias();
       } catch (ParseException ex) {
           Logger.getLogger(LaboratorioDesaUI.class.getName()).log(Level.SEVERE, null, ex);
       } catch (IOException ex) {
           Logger.getLogger(LaboratorioDesaUI.class.getName()).log(Level.SEVERE, null, ex);
       } catch (org.json.simple.parser.ParseException ex) {
           Logger.getLogger(LaboratorioDesaUI.class.getName()).log(Level.SEVERE, null, ex);
       }
  }
  public void llenarLibros(){
       try {
           listaLibros = persistencia.ConsumirListaLibros();
       } catch (ParseException ex) {
           Logger.getLogger(LaboratorioDesaUI.class.getName()).log(Level.SEVERE, null, ex);
       } catch (IOException ex) {
           Logger.getLogger(LaboratorioDesaUI.class.getName()).log(Level.SEVERE, null, ex);
       } catch (org.json.simple.parser.ParseException ex) {
           Logger.getLogger(LaboratorioDesaUI.class.getName()).log(Level.SEVERE, null, ex);
       }
  }
  public void LlenarListaAnios(){
   int conteoAni= 1960;  
  
  for(int i=conteoAni; i<=obtenerFechaActual();i++){
        listaAnios.add(i);
       
  }
   }
 
  public void libroAgregar() throws UnsupportedEncodingException, IOException{
    FacesMessage mensaje =new FacesMessage(); 
   // libroDao.agregar(libro);
    
     
   
    JSONObject datos = new JSONObject();
  
   
    datos.put("titulo", libro.getTitulo());
    datos.put("idautor",libro.getAutor().getIdAutor());
    datos.put("idcategoria", libro.getCategoria().getIdCategoria());
    datos.put("anio", libro.getAnioEdicion());
    datos.put("cantidad", libro.getCantidd());
    BASE64Encoder encoder = new BASE64Encoder();
    datos.put("portada", encoder.encodeBuffer(libro.getPortada()));  
   System.out.println(datos.toJSONString());
   
   
      
       String uri = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/agglibro?nombre="+Base64.encodeBase64String(datos.toJSONString().getBytes());
       uri = uri.replaceAll("\\R", "");
    
    System.out.println(uri);
     System.out.println("File: "+file.getInputstream());
                                                                                     
        try {
             URL url = new URL(uri);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
           
            FacesMessage message = new FacesMessage("Se ha registro un nuevo libro", file.getFileName() + " is uploaded.");
             FacesContext.getCurrentInstance().addMessage(null, message); 
             clear();
            if(conn.getResponseCode()!=200){
                System.out.println("No Se conecto"+conn.getResponseCode());
            }
            
           
            
        } catch (MalformedURLException ex) {
            System.out.println("Error en: "+ex);
        } catch (IOException ex) {
            System.out.println("Error del conn: "+ex);
        }
// listaLibros = libroDao.obtenerLibros();
     
       
   
       
  }
   public void upload() throws UnsupportedEncodingException, IOException {
    
     if(file!=null){
         generarArchivoPrueba();
         libro.setPortada(file.getContents());
         libroAgregar();
         
     }
     
    

    
   
}
   public void mostrarLibro(){
       
     List<Libro>listaLibrosS = new ArrayList();
       try {
           listaLibrosS= persistencia.ConsumirListaLibros();
               for(Libro i: listaLibrosS){
        System.out.println(i.getTitulo());
       System.out.println(i.getAutor().getIdAutor());
       System.out.println(i.getCategoria().getIdCategoria());
       System.out.println(i.getCantidd());
       System.out.println(Arrays.toString(i.getPortada()));   
       }
       } catch (ParseException ex) {
           Logger.getLogger(LaboratorioDesaUI.class.getName()).log(Level.SEVERE, null, ex);
       } catch (IOException ex) {
           Logger.getLogger(LaboratorioDesaUI.class.getName()).log(Level.SEVERE, null, ex);
       } catch (org.json.simple.parser.ParseException ex) {
           Logger.getLogger(LaboratorioDesaUI.class.getName()).log(Level.SEVERE, null, ex);
       }
       System.out.println("LIBROS MIOS");
         for(Libro i: listaLibros){
        System.out.println(i.getTitulo());
       System.out.println(i.getAutor().getIdAutor());
       System.out.println(i.getCategoria().getIdCategoria());
       System.out.println(i.getCantidd());
       System.out.println(Arrays.toString(i.getPortada()));   
         }
      /*
      libro.setPortada(file.getContents());
       System.out.println(libro.getTitulo());
       System.out.println(libro.getAutor().getIdAutor());
       System.out.println(libro.getCategoria().getIdCategoria());
       System.out.println(libro.getCantidd());
       System.out.println(Arrays.toString(libro.getPortada()));   
       System.out.println(file.getFileName());*/
       
   }
  
    public void view(Libro libro){
        try {
         HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();  
         response.getOutputStream().write(libro.getPortada());
         response.getOutputStream().close();
         FacesContext.getCurrentInstance().responseComplete();
        } catch (Exception e) {
        FacesMessage message = new FacesMessage("Error");
        FacesContext.getCurrentInstance().addMessage(null, message);
        }
  
   }
     public void clear(){
   libro.setTitulo(null);
  // libro.setPortada(null);
   libro.setCantidd(0);

   libro.setAnioEdicion(0);
   //libro.setPortada(null);
   
   }
     
  public void descargarimagenes(){
   String ruta = rutaAbsoluta();
     File directorio = new File(ruta+"\\Image");
         if(!directorio.exists()){
             if(directorio.mkdirs()){
                 System.out.println("Directorio creado");
                 for(Libro lib: listaLibros){
        try {
        String nombrePortada = lib.getIdLibro()+"-"+lib.getTitulo();
         FileInputStream fileInputStream = null;
         String  tmpDir=ruta+"\\Image\\"+nombrePortada+".jpg";
	
             
		if (!new File(tmpDir).exists()) {
                FileOutputStream fileOuputStream = new FileOutputStream(tmpDir);
			fileOuputStream.write(lib.getPortada());
			fileOuputStream.close();
                        System.out.println("SE DESCARGO LA IMAGEN CON EXITO");
                }
                      
	
         } catch (Exception e) {
             System.out.println("Error al descargar imagen: "+e);
         }  
  }
             }else{
                 System.out.println("Erro al crear directorio");
             }
         }else{
              for(Libro lib: listaLibros){
        try {
        String nombrePortada = lib.getIdLibro()+"-"+lib.getTitulo();
         FileInputStream fileInputStream = null;
         String  tmpDir=ruta+"\\Image\\"+nombrePortada+".jpg";
	
             
		if (!new File(tmpDir).exists()) {
                FileOutputStream fileOuputStream = new FileOutputStream(tmpDir);
			fileOuputStream.write(lib.getPortada());
			fileOuputStream.close();
                        System.out.println("SE DESCARGO LA IMAGEN CON EXITO");
                }
                      
	
         } catch (Exception e) {
             System.out.println("Error al descargar imagen: "+e);
         }  
  }
         }
  
        
     }
     
  public void CrearArchivoTemporal(){
 
   String ruta = rutaAbsoluta();
   File directorio = new File(ruta+"\\Temporal");
   String fileNameWithOutExt = FilenameUtils.removeExtension(file.getFileName());
   String  tmpDir=ruta+"\\Temporal\\"+fileNameWithOutExt+".jpg";
         if(!directorio.exists()){
             if(directorio.mkdirs()){
                 System.out.println("Directorio creado");
                 
        try {
         if (!new File(tmpDir).exists()) {
                FileOutputStream fileOuputStream = new FileOutputStream(tmpDir);
			fileOuputStream.write(file.getContents());
			fileOuputStream.close();
                       // System.out.println("SE COPIE LA IMAGEN TEMPORAL CORRECTAMENTE");
                }
                      
	
         } catch (Exception e) {
             System.out.println("Error al descargar imagen: "+e);
         }  
 
             }else{
                 System.out.println("Erro al crear directorio");
             }
         }else{System.out.println("CARPETA TEMPORAL YA EXISTE");
              try {
         if (!new File(tmpDir).exists()) {
           //  System.out.println("El archivo no existe en la carpeta temporal");
                FileOutputStream fileOuputStream = new FileOutputStream(tmpDir);
			fileOuputStream.write(file.getContents());
			fileOuputStream.close();
                        //System.out.println("SE COPIE LA IMAGEN TEMPORAL CORRECTAMENTE");
                }
                      
	
         } catch (Exception e) {
             System.out.println("Error al descargar imagen: "+e);
         }  
         }
  
        
     }
	
public String UnioIdMasTitulo(Libro libro) {

    String id= String.valueOf(libro.getIdLibro());
    String respuesta =  id+"-"+libro.getTitulo();
    System.out.println(respuesta);
    return respuesta;
}


public void generarArchivoPrueba(){
       String fileNameWithOutExt = FilenameUtils.removeExtension(file.getFileName());
      CrearArchivoTemporal();
      imagenLibroTemporal = fileNameWithOutExt+".jpg";
}


public void eliminarArchivoTemporal(String nombreArchivo){
   try{

            File archivo = new File("C:\\PruebaAlternativaProyecto\\src\\main\\webapp\\temporal\\"+nombreArchivo+".jpg");

            boolean estatus = archivo.delete();;

            if (!estatus) {

                System.out.println("Error no se ha podido eliminar el  archivo");

           }else{

                System.out.println("Se ha eliminado el archivo exitosamente");

           }

        }catch(Exception e){

           System.out.println(e);

        }

    
}

public String rutaAbsoluta(){
    String rutaBruta = this.getClass().getResource("").getPath();
    rutaBruta = rutaBruta.substring(0, rutaBruta.length() - 28);
    String ruta = rutaBruta + "/";
    //System.out.println( "ruta es:"+rutaBruta);
       return rutaBruta;
   
}
public void men(){
    System.out.println("hola");
}


  public void submitImagen(){
    if(file!=null){
     generarArchivoPrueba();
      
     }     
  }
   public void modificarLibro(){
   
     libro.setIdLibro(idAux);
     libro.setPortada(obtenerPortadLibro(idAux));
     //libro.setPortada(file.getContents());
    // libroDao.modificar(libro);
    
     
    JSONObject datos = new JSONObject();
  
    datos.put("id", libro.getIdLibro());
    datos.put("titulo", libro.getTitulo());
    datos.put("idautor",libro.getAutor().getIdAutor());
    datos.put("idcategoria",libro.getCategoria().getIdCategoria());
    datos.put("anio", libro.getAnioEdicion());
    datos.put("cantidad", libro.getCantidd());
    BASE64Encoder encoder = new BASE64Encoder();
    datos.put("portada", String.valueOf(encoder.encodeBuffer(libro.getPortada())) );  
    
       System.out.println(datos.toJSONString());
       String uri = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/actualizarLibro?nombre="+Base64.encodeBase64String(datos.toJSONString().getBytes());
       uri = uri.replaceAll("\\R", "");
    
 
                                                                                       
        try {
             URL url = new URL(uri);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            
            if(conn.getResponseCode()!=200){
                System.out.println("No Se conecto");
                System.out.println("metodo modificar libro");
            }
            
           
            
        } catch (MalformedURLException ex) {
            System.out.println("Error en: "+ex);
        } catch (IOException ex) {
            System.out.println("Error del conn: "+ex);
        }
       
  llenarLibros();
   descargarimagenes();
}
 
   
   public void submitPortada(){
       if(file!=null){
           libro.setPortada(file.getContents());
           System.out.println("Sub: "+libro.getPortada());
       }
   }
   
public String UnioIdMasTitulo2(Libro libro) {

    String id= String.valueOf(idAux);
    String respuesta =  id+"-"+libro.getTitulo();
    System.out.println(respuesta);
    return respuesta;
}
public void obtenerIdSeleccionadoDeTabla(Libro libro) {
      idAux = libro.getIdLibro();
    }

public byte[] obtenerPortadLibro(int id){
    byte[]bytes = null;
    for(Libro li: listaLibros){
        if(li.getIdLibro()==id){
        bytes = li.getPortada();   
        }
    }
    return bytes;
}

}
