/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.utilidades.Persistencia;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;

import javax.faces.context.FacesContext;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.swing.ImageIcon;
import modelo.dao.AutorDao;
import modelo.dao.LibroDao;

import modelo.entidad.Autor;
import modelo.entidad.Libro;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import javax.servlet.ServletContext;
import org.apache.commons.io.FilenameUtils;


/**
 *
 * @author elcon
 */

import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import modelo.dao.EmpleadoDao;
import modelo.dao.PuestoDao;
import modelo.entidad.Empleado;

import modelo.entidad.Puesto;
import org.apache.commons.net.util.Base64;
import org.json.simple.JSONObject;
@ManagedBean(name ="beanEm")
@RequestScoped
public class EmpleadoBean{

   private static int idAux;
   private Empleado empleado;
   private EmpleadoDao empleadoDao;
   private  PuestoDao puestoDao;
   private List<Puesto> listasPuestos;
   private List<Empleado> listasEmpleados;
   private Persistencia persistenci;

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public List<Puesto> getListasPuestos() {
        return listasPuestos;
    }

    public void setListasPuestos(List<Puesto> listasPuestos) {
        this.listasPuestos = listasPuestos;
    }

    public EmpleadoDao getEmpleadoDao() {
        return empleadoDao;
    }

    public void setEmpleadoDao(EmpleadoDao empleadoDao) {
        this.empleadoDao = empleadoDao;
    }

    public PuestoDao getPuestoDao() {
        return puestoDao;
    }

    public void setPuestoDao(PuestoDao puestoDao) {
        this.puestoDao = puestoDao;
    }

    public List<Empleado> getListasEmpleados() {
        return listasEmpleados;
    }

    public void setListasEmpleados(List<Empleado> listasEmpleados) {
        this.listasEmpleados = listasEmpleados;
    }

    public int getIdAux() {
        return idAux;
    }

    public void setIdAux(int idAux) {
        this.idAux = idAux;
    }

    public Persistencia getPersistenci() {
        return persistenci;
    }

    public void setPersistenci(Persistencia persistenci) {
        this.persistenci = persistenci;
    }
    LaboratorioDesaUI Principal;
    @PostConstruct
    public void init(){
        empleado = new Empleado();
        empleadoDao = new EmpleadoDao();
        puestoDao= new PuestoDao();
        listasEmpleados = new ArrayList();
        listasPuestos= new ArrayList();
        persistenci = new Persistencia();
       llenarListas(); 
       
    }
    
    public void llenarListas(){
      
      
       try {
             listasPuestos = persistenci.ConsumirListaPuestos();
           listasEmpleados= persistenci.ConsumirListaEmpleados();
       } catch (ParseException ex) {
           Logger.getLogger(EmpleadoBean.class.getName()).log(Level.SEVERE, null, ex);
       } catch (IOException ex) {
           Logger.getLogger(EmpleadoBean.class.getName()).log(Level.SEVERE, null, ex);
       } catch (org.json.simple.parser.ParseException ex) {
           Logger.getLogger(EmpleadoBean.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    
    
    public void empleadoAgregar() throws URISyntaxException{
  FacesMessage mensaje =new FacesMessage(); 
 
    
    
   // EmpleadoDao empleadoDao = new EmpleadoDao();
    //empleadoDao.agregar(empleado);
    
    JSONObject datos = new JSONObject();
  
   
    datos.put("nombre", empleado.getNombre());
    datos.put("apellido", empleado.getApellido());
    datos.put("direccion", empleado.getDireccion());
    datos.put("telefono", empleado.getTelefono());
    datos.put("email", empleado.getEmail());
    datos.put("puesto", empleado.getPuesto().getIdPuesto());
   System.out.println(datos.toJSONString());
      
       String uri = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/agregarEmpleado?nombre="+Base64.encodeBase64String(datos.toJSONString().getBytes());
       uri = uri.replaceAll("\\R", "");
    
    System.out.println(uri);
                                                                                       
        try {
             URL url = new URL(uri);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            if(conn.getResponseCode()!=200){
                System.out.println("No Se conecto");
            }
            
           
            
        } catch (MalformedURLException ex) {
            System.out.println("Error en: "+ex);
        } catch (IOException ex) {
            System.out.println("Error del conn: "+ex);
        }
 
    FacesMessage message = new FacesMessage("Se ha registro un nuevo Empleado", empleado.getNombre() + " is uploaded.");
    FacesContext.getCurrentInstance().addMessage(null, message);   
    clear();
        
}
 public void clear(){
  empleado.setNombre(null);
  empleado.setApellido(null);
  empleado.setDireccion(null);
  empleado.setEmail(null);
  empleado.setTelefono(0);
 
   }
 
public void men(){
    System.out.println("hola");
}
public void obtenerIdSeleccionadoDeTabla(Empleado empleado) {
      idAux = empleado.getIdEmpleado();
      System.out.println(idAux);
        
       // System.out.println(persona.toString());
        //ad.modificar(persona);
        //limpiarPersona();
       //  LlenarLista();
    }
  public void modificarEmpleado() {
     empleado.setIdEmpleado(idAux);
   // empleadoDao.modificar(empleado);
    
    
      JSONObject datos = new JSONObject();
      datos.put("idempleado", empleado.getIdEmpleado());
      datos.put("nombre", empleado.getNombre());
      datos.put("apellido", empleado.getApellido());
    datos.put("direccion", empleado.getDireccion()); 
    datos.put("telefono", empleado.getTelefono());
    datos.put("email", empleado.getEmail());
    datos.put("puesto", empleado.getPuesto().getIdPuesto());
       String uri = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/actualizarEmpleado?nombre="+Base64.encodeBase64String(datos.toJSONString().getBytes());
       uri = uri.replaceAll("\\R", "");
    
                                                                                     
        try {
             URL url = new URL(uri);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
           if(conn.getResponseCode()!=200){
                System.out.println("No Se conecto"+conn.getResponseCode());
            }
            
           
            
        } catch (MalformedURLException ex) {
            System.out.println("Error en: "+ex);
        } catch (IOException ex) {
            System.out.println("Error del conn: "+ex);
        }
      llenarListas();
      clear();
    }
}

