/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.utilidades.Persistencia;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import modelo.dao.CategoriaDao;

import modelo.entidad.Categoria;
import org.apache.commons.net.util.Base64;
import org.json.simple.JSONObject;




/**
 *
 * @author elcon
 */
@ManagedBean(name ="bkn_categoria")
@RequestScoped
public class CategoriaBean {

    /**
     * Creates a new instance of LaboratorioDesaUI
     */
  
    private List<Categoria> listaCategorias;
    private Categoria categoria;
    private CategoriaDao categoriaDao;
    private Persistencia persistencia;
       private static int idAux;

  
@PostConstruct
  public void init(){
  listaCategorias= new ArrayList();
  categoria= new Categoria();
  categoriaDao = new CategoriaDao();
  persistencia= new Persistencia();
  llenarListas();
    
  }

    public static int getIdAux() {
        return idAux;
    }

    public static void setIdAux(int idAux) {
        CategoriaBean.idAux = idAux;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public List<Categoria> getListaCategorias() {
        return listaCategorias;
    }

    public void setListaCategorias(List<Categoria> listaCategorias) {
        this.listaCategorias = listaCategorias;
    }

    public CategoriaDao getCategoriaDao() {
        return categoriaDao;
    }

    public void setCategoriaDao(CategoriaDao categoriaDao) {
        this.categoriaDao = categoriaDao;
    }


    
public void llenarListas(){
        try {
            listaCategorias = persistencia.ConsumirListaCategorias();
        } catch (ParseException ex) {
            Logger.getLogger(CategoriaBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CategoriaBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (org.json.simple.parser.ParseException ex) {
            Logger.getLogger(CategoriaBean.class.getName()).log(Level.SEVERE, null, ex);
        }
}
public void categoriaAgregar(){
   //categoriaDao = new CategoriaDao();
  // categoriaDao.agregar(categoria);
  JSONObject datos = new JSONObject();
    datos.put("categoria", categoria.getCategoria());
    
    String uri = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/aggcategoria?nombre="+Base64.encodeBase64String(datos.toJSONString().getBytes());
    
        try {
            URL url = new URL(uri);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            if(conn.getResponseCode()!=200){
                System.out.println("No Se conecto");
            }
            
            
        } catch (MalformedURLException ex) {
            System.out.println("Error en: "+ex);
        } catch (IOException ex) {
            System.out.println("Error del conn: "+ex);
        }
   
    FacesMessage mensaje =new FacesMessage(); 
   // puestoDao = new PuestoDao();
  
  
    FacesMessage message = new FacesMessage("Se ha registro un nueva categoria", categoria.getCategoria() + " is uploaded.");
    FacesContext.getCurrentInstance().addMessage(null, message); 
    clear();
}
 public void clear(){
  categoria.setCategoria(null);
   
   }
   public void obtenerIdSeleccionadoDeTabla(Categoria categoria) {
      idAux = categoria.getIdCategoria();
    }
  public void modificarCategoria() throws java.text.ParseException, IOException{
    categoria.setIdCategoria(idAux);
    //categoriaDao.modificar(categoria);
    
      
    JSONObject datos = new JSONObject();
      datos.put("idcategoria", categoria.getIdCategoria());
      datos.put("nombre", categoria.getCategoria());
  
       String uri = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/actualizarCate?nombre="+Base64.encodeBase64String(datos.toJSONString().getBytes());
       uri = uri.replaceAll("\\R", "");
      System.out.println(uri);
                                                                                     
        try {
             URL url = new URL(uri);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
           if(conn.getResponseCode()!=200){
                System.out.println("No Se conecto"+conn.getResponseCode());
            }
            
           
            
        } catch (MalformedURLException ex) {
            System.out.println("Error en: "+ex);
        } catch (IOException ex) {
            System.out.println("Error del conn: "+ex);
        }
   llenarListas();
   clear();
     
 
}
}
