/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.utilidades.Persistencia;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.apache.commons.net.util.Base64;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import modelo.dao.AutorDao;

import modelo.entidad.Autor;
import org.json.simple.parser.ParseException;

import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.dao.LibroDao;
import modelo.entidad.Libro;
import org.json.simple.JSONObject;



/**
 *
 * @author elcon
 */
@ManagedBean(name ="bkn_autor")
@RequestScoped
public class AutorBean {

    /**
     * Creates a new instance of LaboratorioDesaUI
     */
  
    private List<Autor> lista;
    private AutorDao autorDao;
    private Autor autor;
    private Persistencia persistencia;
   private static int idAux;
@PostConstruct
  public void init(){
  lista= new ArrayList();
  autor = new Autor();
  autorDao = new AutorDao();
  persistencia = new Persistencia();
  
        try {
            llenarListas();
        } catch (java.text.ParseException ex) {
            Logger.getLogger(AutorBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AutorBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    
  }

    public static int getIdAux() {
        return idAux;
    }

    public static void setIdAux(int idAux) {
        AutorBean.idAux = idAux;
    }

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public AutorDao getAutorDao() {
        return autorDao;
    }

    public void setAutorDao(AutorDao autorDao) {
        this.autorDao = autorDao;
    }

    public List<Autor> getLista() {
        return lista;
    }

    public void setLista(List<Autor> lista) {
        this.lista = lista;
    }
    
public void llenarListas() throws java.text.ParseException, IOException{
    try {
            lista= persistencia.autores();
          
        } catch (ParseException ex) {
            Logger.getLogger(AutorBean.class.getName()).log(Level.SEVERE, null, ex);
        }
   
}
public void autorAgregar(){
  JSONObject datos = new JSONObject();
    datos.put("autor", autor.getAutor());
    
    String uri = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/aggautor?nombre="+Base64.encodeBase64String(datos.toJSONString().getBytes());
    
        try {
            URL url = new URL(uri);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            if(conn.getResponseCode()!=200){
                System.out.println("No Se conecto");
            }
            
            
        } catch (MalformedURLException ex) {
            System.out.println("Error en: "+ex);
        } catch (IOException ex) {
            System.out.println("Error del conn: "+ex);
        }
    
    //autorDao = new AutorDao();
    //autorDao.agregar(autor);
       FacesMessage mensaje =new FacesMessage(); 
   // puestoDao = new PuestoDao();
  
  
    FacesMessage message = new FacesMessage("Se ha registro un nuevO Autor", autor.getAutor() + " is uploaded.");
    FacesContext.getCurrentInstance().addMessage(null, message); 
    clear();
}
 public void clear(){
   autor.setAutor(null);
 
   }
 public void mostrar() throws java.text.ParseException, IOException{
     List<Autor>autores = new ArrayList();
     System.out.println("hola");
        try {
            autores = persistencia.autores();
            for(Autor a: autores){
                System.out.println(a.getAutor());
            }
            
        } catch (ParseException ex) {
            Logger.getLogger(AutorBean.class.getName()).log(Level.SEVERE, null, ex);
        }
     
 }
 
 public void actualizarCantidad(){
   LibroDao libroDao = new LibroDao();
   Libro libro = new Libro();
   libro.setIdLibro(14);
   libro.setCantidd(1);
   libroDao.modificarWeb(libro.getIdLibro(),libro.getCantidd());
 }
 public void obtenerIdSeleccionadoDeTabla(Autor autor) {
      idAux = autor.getIdAutor();
    }
  public void modificarAutor() throws java.text.ParseException, IOException{
    
    
     autor.setIdAutor(idAux);
    //autorDao.modificar(autor);
     //libro.setPortada(file.getContents());
   JSONObject datos = new JSONObject();
      datos.put("idautor", autor.getIdAutor());
      datos.put("nombre", autor.getAutor());
      System.out.println(datos.toJSONString());
       String uri = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/actualizarAutor?nombre="+Base64.encodeBase64String(datos.toJSONString().getBytes());
       uri = uri.replaceAll("\\R", "");
    
                                                                                     
        try {
             URL url = new URL(uri);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
           if(conn.getResponseCode()!=200){
                System.out.println("No Se conecto"+conn.getResponseCode());
            }
            
           
            
        } catch (MalformedURLException ex) {
            System.out.println("Error en: "+ex);
        } catch (IOException ex) {
            System.out.println("Error del conn: "+ex);
        }
     llenarListas();
     clear();  
  
}
   
}