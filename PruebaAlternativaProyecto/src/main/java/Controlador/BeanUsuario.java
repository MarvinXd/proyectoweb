/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.utilidades.Persistencia;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;

import javax.faces.context.FacesContext;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.swing.ImageIcon;
import modelo.dao.AutorDao;
import modelo.dao.LibroDao;

import modelo.entidad.Autor;
import modelo.entidad.Libro;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import javax.servlet.ServletContext;
import org.apache.commons.io.FilenameUtils;

import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import modelo.dao.EmpleadoDao;
import modelo.dao.UsuarioDao;
import modelo.entidad.Usuario;
import org.apache.commons.net.util.Base64;
import org.json.simple.JSONObject;
/**
 *
 * @author elcon
 */
@ManagedBean(name ="bkn_Libro2")
@RequestScoped
public class BeanUsuario {
  
    private Usuario usuario;
    private UsuarioDao usuarioDao;
    private Persistencia persistencia;
    private List<Usuario>listaUsuarios;
    private static int idAux;
@PostConstruct
  public void init(){
    usuario = new Usuario();
    usuarioDao = new UsuarioDao();
    persistencia= new Persistencia();
    llenarListaUsuario();

    
  }

    public static int getIdAux() {
        return idAux;
    }

    public static void setIdAux(int idAux) {
        BeanUsuario.idAux = idAux;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public UsuarioDao getUsuarioDao() {
        return usuarioDao;
    }

    public void setUsuarioDao(UsuarioDao usuarioDao) {
        this.usuarioDao = usuarioDao;
    }

    public List<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }
   
    public void llenarListaUsuario(){
        try {
            listaUsuarios = persistencia.ConsumirListaUsuario();
        } catch (ParseException ex) {
            Logger.getLogger(BeanUsuario.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BeanUsuario.class.getName()).log(Level.SEVERE, null, ex);
        } catch (org.json.simple.parser.ParseException ex) {
            Logger.getLogger(BeanUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
      public void usuarioAgregar(){
   
    FacesMessage mensaje =new FacesMessage(); 
    
    JSONObject datos = new JSONObject();
  
   
    datos.put("nombre", usuario.getNombre());
    datos.put("apellido",  usuario.getApellido());
    datos.put("telefono", usuario.getTelefono());
    datos.put("estado",  usuario.getEstado());
   System.out.println(datos.toJSONString());
      
       String uri = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/aggusuario?nombre="+Base64.encodeBase64String(datos.toJSONString().getBytes());
       uri = uri.replaceAll("\\R", "");
    
    System.out.println(uri);
                                                                                       
        try {
             URL url = new URL(uri);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            if(conn.getResponseCode()!=200){
                System.out.println("No Se conecto");
            }
            
           
            
        } catch (MalformedURLException ex) {
            System.out.println("Error en: "+ex);
        } catch (IOException ex) {
            System.out.println("Error del conn: "+ex);
        }
 
   
    FacesMessage message = new FacesMessage("Se ha registro un nuevo Empleado",usuario.getNombre() + " is uploaded.");
    FacesContext.getCurrentInstance().addMessage(null, message);   
    clear();
         
        
}
 public void clear(){
  usuario.setNombre(null);
  usuario.setApellido(null);
  usuario.setTelefono(0);
 
   }
public void men(){
    System.out.println("hola");
}

  public void obtenerIdSeleccionadoDeTabla(Usuario usuario) {
      idAux = usuario.getIdUsuario();
    }
  public void modificarUsuario() throws java.text.ParseException, IOException{
    usuario.setIdUsuario(idAux);
   // usuarioDao.modificar(usuario);

    
    JSONObject datos = new JSONObject();
      datos.put("idusuario", usuario.getIdUsuario());
      datos.put("nombre", usuario.getNombre());
        datos.put("apellido", usuario.getApellido());
       datos.put("telefono", usuario.getTelefono());
     datos.put("estado", usuario.getEstado());
   
       String uri = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/actualizarUsuario?nombre="+Base64.encodeBase64String(datos.toJSONString().getBytes());
       uri = uri.replaceAll("\\R", "");
    
                                                                                     
        try {
             URL url = new URL(uri);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
           if(conn.getResponseCode()!=200){
                System.out.println("No Se conecto"+conn.getResponseCode());
            }
            
           
            
        } catch (MalformedURLException ex) {
            System.out.println("Error en: "+ex);
        } catch (IOException ex) {
            System.out.println("Error del conn: "+ex);
        }
     llenarListaUsuario();
     clear();
     
}
}
