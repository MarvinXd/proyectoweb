/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.util;


/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author ADMIN
 */
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtil {

    public static final ThreadLocal session =new ThreadLocal();
    public static final SessionFactory sessionFactory;

    static {
          try {
            sessionFactory = new Configuration().configure().buildSessionFactory();
          } catch (Throwable ex) {
               throw new ExceptionInInitializerError(ex);
          }     

    }

    public static Session currentSession () throws HibernateException {

        Session s = (Session) session.get ();
            if(s == null){
                s = sessionFactory.openSession ();
                session.set(s);
             }
            return s;

        }

    public static void closeSession() throws HibernateException {

        Session s = (Session) session.get ();
        if(s != null) {
            s.close();
        }
        session.set(null);

    }

}
