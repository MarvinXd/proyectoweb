/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import java.util.List;
import modelo.entidad.Empleado;
import modelo.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author Marvin
 */
public class EmpleadoDao {
    public List<Empleado> listaEmpleados() {
         Session sesion = null;
         List<Empleado> lista = null;
    
        String hql = "FROM Empleado";
        try {
        //SessionFactory sessionFactory = HibernateUtil.createSessionFactory();
        
        //sesion = sessionFactory.openSession();
        sesion = HibernateUtil.currentSession();
        if(!sesion.isOpen()){
            sesion = HibernateUtil.sessionFactory.openSession();
            sesion.beginTransaction();
            lista = sesion.createQuery(hql).list();
            sesion.getTransaction().commit();
            System.out.println(sesion+"Con exito");
             sesion.close();
        }else{
             sesion.beginTransaction();
            lista = sesion.createQuery(hql).list();
            sesion.getTransaction().commit();
            System.out.println(sesion+"Con exito");
             sesion.close();
        }
        
            
            
        } catch (HibernateException e) {
            System.out.println("Error "+e);
            sesion.getTransaction().rollback();
        }
        return lista;
    }

 

    public void agregar(Empleado empleado) {
        Session sesion = null;
        try {
             //SessionFactory sessionFactory = HibernateUtil.createSessionFactory();
            //sesion = sessionFactory.openSession();
            sesion = HibernateUtil.currentSession();
            if(!sesion.isOpen()){
            sesion = HibernateUtil.sessionFactory.openSession();    
             sesion.beginTransaction();
            sesion.save(empleado);
            sesion.getTransaction().commit();
             sesion.close();
            }else{
            sesion.beginTransaction();
            sesion.save(empleado);
            sesion.getTransaction().commit();
             sesion.close();
            }
           
           
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        } 
    } 
    public void modificar(Empleado empleado) {
        Session sesion = null;
        try {
        sesion = HibernateUtil.currentSession();
        if(!sesion.isOpen()){
            sesion = HibernateUtil.sessionFactory.openSession();
            
            sesion.beginTransaction();
            sesion.update(empleado);
            sesion.getTransaction().commit();
            sesion.close();
        }else{
            sesion.beginTransaction();
            sesion.update(empleado);
            sesion.getTransaction().commit();   
             sesion.close();
        }
        
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        }
    }
    
}
