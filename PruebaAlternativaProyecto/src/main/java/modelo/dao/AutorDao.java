/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;


import java.util.List;
import modelo.entidad.Autor;
import modelo.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author ADMIN
 */
public class AutorDao {
   
    
    public List<Autor> listarAutores() {
         Session sesion = null;
         List<Autor> lista = null;
    
        String hql = "FROM Autor";
        try {
        //SessionFactory sessionFactory = HibernateUtil.createSessionFactory();
        
        //sesion = sessionFactory.openSession();
        sesion = HibernateUtil.currentSession();
        if(!sesion.isOpen()){
          sesion= HibernateUtil.sessionFactory.openSession();
            sesion.beginTransaction();
            lista = sesion.createQuery(hql).list();
            sesion.getTransaction().commit();
            System.out.println(sesion+"Con exito lista autores ,estaba cerrada y se abrio la sesion");
             sesion.close();
        }else{
          sesion.beginTransaction();
            lista = sesion.createQuery(hql).list();
            sesion.getTransaction().commit();
            System.out.println(sesion+"YA ESTABA ABIERTA Y SE CONSUMIO PERFECTAMENTE");   
              sesion.close();
        }
      
            
            
        } catch (HibernateException e) {
            System.out.println("Error "+e);
            sesion.getTransaction().rollback();
        }
        return lista;
    }

 

    public void agregar(Autor autor) {
        Session sesion = null;
        try {
             //SessionFactory sessionFactory = HibernateUtil.createSessionFactory();
            //(sesion = sessionFactory.openSession();
            sesion = HibernateUtil.currentSession();
            if(!sesion.isOpen()){
                sesion= HibernateUtil.sessionFactory.openSession(); 
            sesion.beginTransaction();
            sesion.save(autor);
            sesion.getTransaction().commit(); 
            sesion.close();
            
            }else{
              sesion.beginTransaction();
            sesion.save(autor);
            sesion.getTransaction().commit();   
             sesion.close();
            }
           
           
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        } 
    }

    public void modificar(Autor autor) {
        Session sesion = null;
        try {
        sesion = HibernateUtil.currentSession();
        if(!sesion.isOpen()){
            sesion = HibernateUtil.sessionFactory.openSession();
            
            sesion.beginTransaction();
            sesion.update(autor);
            sesion.getTransaction().commit();
            sesion.close();
        }else{
            sesion.beginTransaction();
            sesion.update(autor);
            sesion.getTransaction().commit();   
             sesion.close();
        }
        
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        }
    }
/*
    public void eliminar(Persona persona) {
        Session sesion = null;
        try {
            sesion = HibernateUtil.createSessionFactory().openSession();
            sesion.beginTransaction();
            sesion.delete(persona);
            sesion.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }
   
    public void agregar(Persona persona) {
        Session sesion = null;
        try {
            sesion = HibernateUtil.createSessionFactory().openSession();
            sesion.beginTransaction();
            sesion.save(persona);
            sesion.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }

    public void modificar(Persona persona) {
        Session sesion = null;
        try {
             sesion = sessionFactory.openSession();
        sesion.beginTransaction();
            sesion.update(persona);
            sesion.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }

    public void eliminar(Persona persona) {
        Session sesion = null;
        try {
            sesion = HibernateUtil.createSessionFactory().openSession();
            sesion.beginTransaction();
            sesion.delete(persona);
            sesion.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }
*/
   
}

