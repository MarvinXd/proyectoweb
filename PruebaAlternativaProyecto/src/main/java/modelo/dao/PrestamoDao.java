/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import modelo.entidad.Prestamo;
import modelo.util.HibernateUtil;
import static modelo.util.HibernateUtil.session;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Marvin
 */
public class PrestamoDao {
     static Session sesionn = HibernateUtil.currentSession();
  public List<Prestamo> listarPrestamos() {
    
         Session sesion = null;
         List<Prestamo> lista = null;
    
        String hql = "FROM Prestamo";
        try {
        //SessionFactory sessionFactory = HibernateUtil.createSessionFactory();
        
        //sesion = sessionFactory.openSession();
        sesion = HibernateUtil.currentSession();
         if(!sesion.isOpen()){
             sesion = HibernateUtil.sessionFactory.openSession();
               sesion.beginTransaction();
            lista = sesion.createQuery(hql).list();
            sesion.getTransaction().commit();
            System.out.println(sesion+"Con exito");  
            sesion.close();
            
         }else{
            sesion.beginTransaction();
            lista = sesion.createQuery(hql).list();
            sesion.getTransaction().commit();
            System.out.println(sesion+"Con exito");  
            sesion.close();
         }
        
            
            
        } catch (HibernateException e) {
            System.out.println("Error "+e);
            sesion.getTransaction().rollback();
        }
        return lista;
    }
   @SuppressWarnings("unchecked")
public List<Prestamo> obtenerPrestamos() throws HibernateException {
   List<Prestamo> lista = null;
    if (sesionn==null){
        sesionn = HibernateUtil.sessionFactory.openSession(); 
        sesionn.beginTransaction();
        System.out.println("Sesion listaPrestamo es null y se inicio correctamente");
        lista = sesionn.createQuery("FROM Prestamo").list();
        sesionn.getTransaction().commit();
        sesionn.close();
      }
    if (!sesionn.isOpen()){
        System.out.println("ESTA CERRRADO");
        sesionn = HibernateUtil.sessionFactory.openSession();
        sesionn.beginTransaction();
        System.out.println("Sesion listaPrestamo estaba cerrrado  y se inicio correctamente");
       lista = sesionn.createQuery("FROM Prestamo").list();
        sesionn.getTransaction().commit();
         sesionn.close();
        }
      else{
          sesionn.beginTransaction();
         System.out.println("Sesion listaPrestamo ya estaba abierto y se cerrara correctamente");
       lista = sesionn.createQuery("FROM Prestamo").list();
         sesionn.getTransaction().commit();
         sesionn.close();
         }
   
       
      
      
    return  lista;
}
 public void modificarEstadoPrestamo(int id,String estado){
      Session sesion = null;
        try {
        sesion = HibernateUtil.currentSession();
        if(!sesion.isOpen()){
            sesion = HibernateUtil.sessionFactory.openSession();
              
         sesion.getTransaction().begin();
         Prestamo prestamo = ( Prestamo) sesion.get( Prestamo.class, id);
         prestamo.setEstado(estado);
       sesion.getTransaction().commit();
       sesion.close();
        }else{
           sesion = HibernateUtil.sessionFactory.openSession();
                
         sesion.getTransaction().begin();
            Prestamo prestamo = ( Prestamo) sesion.get( Prestamo.class, id);
         prestamo.setEstado(estado);
       sesion.getTransaction().commit();
       sesion.close();
        }
        
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        }
        
        
      
   }
  
  /*
public List<Prestamo> listarPrestamosConJoin() {
         Session sesion = null;
         List<Prestamo> lista = null;
    
        String hql = "FROM Prestamo p WHERE Id_prestamo = 1";
        try {
        //SessionFactory sessionFactory = HibernateUtil.createSessionFactory();
        
        //sesion = sessionFactory.openSession();
        sesion = HibernateUtil.currentSession();
        sesion.beginTransaction();
        Query query = sesion.createQuery(hql);
        Prestamo prestamo = new Prestamo();
          for (Iterator it = query.iterate(); it.hasNext();) {
         Object[] row = (Object[]) it.next();
          prestamo.getLibro().setTitulo((String) row[0]);
            prestamo.getEmpleado().setNombre((String) row[1]);
           prestamo.getUsuario().setNombre((String) row[2]);
           prestamo.setFechaSalida((Date) row[3]);
           prestamo.setEstado((String) row[3]);
           lista.add(prestamo);
          }
            
        } catch (HibernateException e) {
            System.out.println("Error "+e);
            sesion.getTransaction().rollback();
        }
        return lista;
    }
 */

    public void agregar(Prestamo prestamo) {
         sesionn = null;
        try {
             //SessionFactory sessionFactory = HibernateUtil.createSessionFactory();
            //sesion = sessionFactory.openSession();
            
            sesionn = HibernateUtil.currentSession();
            if(!sesionn.isOpen()){
              sesionn=  HibernateUtil.sessionFactory.openSession();
            sesionn.beginTransaction();
            sesionn.save(prestamo);
            
            sesionn.getTransaction().commit();  
             sesionn.close();
            }else{
            sesionn.beginTransaction();
            sesionn.save(prestamo);
            sesionn.getTransaction().commit(); 
            sesionn.close();
            }
         
           
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesionn.getTransaction().rollback();
        }
    }   
}
