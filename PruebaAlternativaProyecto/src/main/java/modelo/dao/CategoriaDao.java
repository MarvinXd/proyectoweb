/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import java.util.List;
import modelo.entidad.Categoria;
import modelo.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author ADMIN
 */
public class CategoriaDao {
   
    
    
    public List<Categoria> listarCategorias() {
       Session sesion = null;
         List<Categoria> lista = null;
    
        String hql = "FROM Categoria";
        try {
        //SessionFactory sessionFactory = HibernateUtil.createSessionFactory();
        
        //sesion = sessionFactory.openSession();
        sesion = HibernateUtil.currentSession();
         if(!sesion.isOpen()){
             sesion = HibernateUtil.sessionFactory.openSession();
             sesion.beginTransaction();
            lista = sesion.createQuery(hql).list();
            sesion.getTransaction().commit();
            System.out.println(sesion+"Con exito");
            sesion.close();
         }else{
              sesion.beginTransaction();
            lista = sesion.createQuery(hql).list();
            sesion.getTransaction().commit();
            System.out.println(sesion+"Con exito");
            sesion.close();
         }
        
      
            
        } catch (HibernateException e) {
            System.out.println("Error "+e);
            sesion.getTransaction().rollback();
        } 
        return lista;
    }

 

    public void agregar(Categoria categoria) {
         Session sesion = null;
        try {
             //SessionFactory sessionFactory = HibernateUtil.createSessionFactory();
            //sesion = sessionFactory.openSession();
            sesion = HibernateUtil.currentSession();
            if(!sesion.isOpen()){
                 sesion = HibernateUtil.sessionFactory.openSession();
                 sesion.beginTransaction();
            sesion.save(categoria);
            sesion.getTransaction().commit();
            sesion.close();
            }else{
                 sesion.beginTransaction();
            sesion.save(categoria);
            sesion.getTransaction().commit();
            sesion.close();
            }
            
         
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        } 
    }
    
    public void modificar(Categoria categoria) {
        Session sesion = null;
        try {
        sesion = HibernateUtil.currentSession();
        if(!sesion.isOpen()){
            sesion = HibernateUtil.sessionFactory.openSession();
            
            sesion.beginTransaction();
            sesion.update(categoria);
            sesion.getTransaction().commit();
            sesion.close();
        }else{
            sesion.beginTransaction();
            sesion.update(categoria);
            sesion.getTransaction().commit();   
             sesion.close();
        }
        
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        }
    }
/*
    public void modificar(Persona persona) {
        Session sesion = null;
        try {
             sesion = sessionFactory.openSession();
        sesion.beginTransaction();
            sesion.update(persona);
            sesion.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }

    public void eliminar(Persona persona) {
        Session sesion = null;
        try {
            sesion = HibernateUtil.createSessionFactory().openSession();
            sesion.beginTransaction();
            sesion.delete(persona);
            sesion.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }
   
    public void agregar(Persona persona) {
        Session sesion = null;
        try {
            sesion = HibernateUtil.createSessionFactory().openSession();
            sesion.beginTransaction();
            sesion.save(persona);
            sesion.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }

    public void modificar(Persona persona) {
        Session sesion = null;
        try {
             sesion = sessionFactory.openSession();
        sesion.beginTransaction();
            sesion.update(persona);
            sesion.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }

    public void eliminar(Persona persona) {
        Session sesion = null;
        try {
            sesion = HibernateUtil.createSessionFactory().openSession();
            sesion.beginTransaction();
            sesion.delete(persona);
            sesion.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }
*/ 
}
