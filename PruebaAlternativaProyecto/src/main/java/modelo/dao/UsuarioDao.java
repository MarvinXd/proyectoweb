/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import java.util.List;
import modelo.entidad.Usuario;
import modelo.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author Marvin
 */
public class UsuarioDao {
     public List<Usuario> listarUsuarios() {
         Session sesion = null;
         List<Usuario> lista = null;
    
        String hql = "FROM Usuario";
        try {
        //SessionFactory sessionFactory = HibernateUtil.createSessionFactory();
        
        //sesion = sessionFactory.openSession();
        sesion = HibernateUtil.currentSession();
        if(!sesion.isOpen()){
             sesion = HibernateUtil.sessionFactory.openSession();
          sesion.beginTransaction();
            lista = sesion.createQuery(hql).list();
            sesion.getTransaction().commit();
            System.out.println(sesion+"Con exito"); 
            sesion.close();
        }else{
             sesion.beginTransaction();
            lista = sesion.createQuery(hql).list();
            sesion.getTransaction().commit();
              sesion.close();
        }
        
            
            
        } catch (HibernateException e) {
            System.out.println("Error "+e);
            sesion.getTransaction().rollback();
        }
        return lista;
    }

 

    public void agregar(Usuario usuario) {
        Session sesion = null;
        try {
             //SessionFactory sessionFactory = HibernateUtil.createSessionFactory();
            //sesion = sessionFactory.openSession();
            sesion = HibernateUtil.currentSession();
            if(!sesion.isOpen()){
            sesion = HibernateUtil.sessionFactory.openSession();
            sesion.beginTransaction();
            sesion.save(usuario);
            sesion.getTransaction().commit();
            sesion.close();
        }else{
            sesion.beginTransaction();
            sesion.save(usuario);
            sesion.getTransaction().commit();  
              sesion.close();
            }
            
           
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        }
    } 
    
    public void modificar(Usuario usuario) {
        Session sesion = null;
        try {
        sesion = HibernateUtil.currentSession();
        if(!sesion.isOpen()){
            sesion = HibernateUtil.sessionFactory.openSession();
            
            sesion.beginTransaction();
            sesion.update(usuario);
            sesion.getTransaction().commit();
            sesion.close();
        }else{
            sesion.beginTransaction();
            sesion.update(usuario);
            sesion.getTransaction().commit();   
             sesion.close();
        }
        
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        }
    }
}
