/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.dao;

import java.util.List;
import modelo.entidad.Libro;
import modelo.util.HibernateUtil;
import static modelo.util.HibernateUtil.session;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author ADMIN
 */
public class LibroDao {
    static Session sesionn = HibernateUtil.currentSession();
     public List<Libro> listarLibros() {
       Session sesion = null;
         List<Libro> lista = null;
    
        String hql = "FROM Libro";
        try {
        //SessionFactory sessionFactory = HibernateUtil.createSessionFactory();
        
        //sesion = sessionFactory.openSession();
        sesion = HibernateUtil.currentSession();
         if(!sesion.isOpen()){
            sesion = HibernateUtil.sessionFactory.openSession(); 
            sesion.beginTransaction();
            lista = sesion.createQuery(hql).list();
            sesion.getTransaction().commit();
            System.out.println(sesion+"Con exito estaba cerrada");
            sesion.close();
         }else{
           sesion.beginTransaction();
            lista = sesion.createQuery(hql).list();
            sesion.getTransaction().commit();
            System.out.println(sesion+"Con exito ya estaba abierta");  
            sesion.close();
         }
        
           
            
        } catch (HibernateException e) {
            System.out.println("Error "+e);
            sesion.getTransaction().rollback();
        }
        return lista;
    }
      @SuppressWarnings("unchecked")
public List<Libro> obtenerLibros() throws HibernateException {
   List<Libro> lista = null;
    if (sesionn==null){
        sesionn = HibernateUtil.sessionFactory.openSession(); 
        sesionn.beginTransaction();
        System.out.println("Sesion listaPrestamo es null y se inicio correctamente");
        lista = sesionn.createQuery("FROM Libro").list();
        sesionn.getTransaction().commit();
        sesionn.close();
      }
    if (!sesionn.isOpen()){
        System.out.println("ESTA CERRRADO");
        sesionn = HibernateUtil.sessionFactory.openSession();
        sesionn.beginTransaction();
        System.out.println("Sesion listaPrestamo estaba cerrrado  y se inicio correctamente");
       lista = sesionn.createQuery("FROM Libro").list();
        sesionn.getTransaction().commit();
        sesionn.close();
        }
      else{
          sesionn.beginTransaction();
         System.out.println("Sesion listaPrestamo ya estaba abierto y se cerrara correctamente");
       lista = sesionn.createQuery("FROM Libro").list();
         sesionn.getTransaction().commit();
         sesionn.close();
         }
   
       
      
      
    return  lista;
}
  public void agregar(Libro libro) {
         sesionn = null;
        try {
             //SessionFactory sessionFactory = HibernateUtil.createSessionFactory();
            //sesion = sessionFactory.openSession();
            
            sesionn = HibernateUtil.currentSession();
            if(!sesionn.isOpen()){
              sesionn=  HibernateUtil.sessionFactory.openSession();
            sesionn.beginTransaction();
            sesionn.save(libro);
            
            sesionn.getTransaction().commit();  
             sesionn.close();
            }else{
            sesionn.beginTransaction();
            sesionn.save(libro);
            sesionn.getTransaction().commit(); 
            sesionn.close();
            }
         
           
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesionn.getTransaction().rollback();
        }
    }   

   public void modificar(Libro libro) {
        Session sesion = null;
        try {
        sesion = HibernateUtil.currentSession();
        if(!sesion.isOpen()){
            sesion = HibernateUtil.sessionFactory.openSession();
            
            sesion.beginTransaction();
            sesion.update(libro);
            sesion.getTransaction().commit();
            sesion.close();
        }else{
            sesion.beginTransaction();
            sesion.update(libro);
            sesion.getTransaction().commit();   
             sesion.close();
        }
        
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        }
    }
   
  
   
   public void modificarWeb(int id,int cantidad){
      Session sesion = null;
        try {
        sesion = HibernateUtil.currentSession();
        if(!sesion.isOpen()){
            sesion = HibernateUtil.sessionFactory.openSession();
              
         sesion.getTransaction().begin();
         Libro libro = (Libro) sesion.get(Libro.class, id);
         libro.setCantidd(cantidad);
       sesion.getTransaction().commit();
       sesion.close();
        }else{
           sesion = HibernateUtil.sessionFactory.openSession();
                
         sesion.getTransaction().begin();
         Libro libro = (Libro) sesion.get(Libro.class, id);
         libro.setCantidd(cantidad);
       sesion.getTransaction().commit();
       sesion.close();
        }
        
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        }
        
        
      
   }
/*
    public void eliminar(Persona persona) {
        Session sesion = null;
        try {
            sesion = HibernateUtil.createSessionFactory().openSession();
            sesion.beginTransaction();
            sesion.delete(persona);
            sesion.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }
   
    public void agregar(Persona persona) {
        Session sesion = null;
        try {
            sesion = HibernateUtil.createSessionFactory().openSession();
            sesion.beginTransaction();
            sesion.save(persona);
            sesion.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }

    public void modificar(Persona persona) {
        Session sesion = null;
        try {
             sesion = sessionFactory.openSession();
        sesion.beginTransaction();
            sesion.update(persona);
            sesion.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }

    public void eliminar(Persona persona) {
        Session sesion = null;
        try {
            sesion = HibernateUtil.createSessionFactory().openSession();
            sesion.beginTransaction();
            sesion.delete(persona);
            sesion.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            sesion.getTransaction().rollback();
        } finally {
            if (sesion != null) {
                sesion.close();
            }
        }
    }
*/
}
