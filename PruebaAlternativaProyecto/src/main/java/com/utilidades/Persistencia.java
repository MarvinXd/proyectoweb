/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utilidades;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import static javassist.CtMethod.ConstParameter.string;
import modelo.entidad.Autor;
import modelo.entidad.Categoria;
import modelo.entidad.Empleado;
import modelo.entidad.Usuario;
import modelo.entidad.Libro;
import modelo.entidad.Prestamo;
import modelo.entidad.Puesto;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import sun.misc.BASE64Decoder;
/**
 *
 * @author Marvin
 */
public class Persistencia {
  
    
    public ArrayList<Autor> autores() throws ParseException, MalformedURLException, IOException, org.json.simple.parser.ParseException{
        
         
        String responseString = "";
        String outputString = "";
        String wsURL = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/autores";
          ArrayList<Autor> aut = new ArrayList();
        try {
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection)connection;

            if(httpConn.getResponseCode()!=200){
                System.out.println("Parece que no me conecte");
                throw new RuntimeException("No se pudo conectar "+httpConn.getInputStream());
            }

            InputStreamReader isr = null;
            isr = new InputStreamReader(httpConn.getInputStream());
            BufferedReader in = new BufferedReader(isr);

            while ((responseString = in.readLine()) != null) {
            outputString = outputString + responseString;        
            }
            
            byte[] data = org.apache.commons.net.util.Base64.decodeBase64(outputString.getBytes());
            String dataJson = new String(data);
            
            JSONParser parser = new JSONParser();
            //jsonResp = new JSONObject();
                        
            JSONArray jsonResp = (JSONArray) parser.parse(dataJson);
          

            for(int i=0; i<jsonResp.size();i++){
                Autor autg = new Autor();
                JSONObject jm = new JSONObject ();
                jm = (JSONObject) jsonResp.get(i);
                
                int nu = Integer.parseInt(jm.get("ID").toString());
                String nombre = jm.get("Nombre").toString();
                
                autg.setIdAutor(nu);
                autg.setAutor(nombre);                
                aut.add(autg);
            }
          
                         
        }catch (IOException ex) {
            System.out.println("Error en: "+ex);
        } 
        
        
        
        return aut;
        
    }
     public ArrayList<Prestamo> ConsumirListaPrestamos() throws ParseException, MalformedURLException, IOException, org.json.simple.parser.ParseException{
        
         
        String responseString = "";
        String outputString = "";
        String wsURL = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/prestamos";
          ArrayList<Prestamo> p = new ArrayList();
        try {
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection)connection;

            if(httpConn.getResponseCode()!=200){
                System.out.println("Parece que no me conecte");
                throw new RuntimeException("No se pudo conectar "+httpConn.getInputStream());
            }

            InputStreamReader isr = null;
            isr = new InputStreamReader(httpConn.getInputStream());
            BufferedReader in = new BufferedReader(isr);

            while ((responseString = in.readLine()) != null) {
            outputString = outputString + responseString;        
            }
            
            byte[] data = org.apache.commons.net.util.Base64.decodeBase64(outputString.getBytes());
            String dataJson = new String(data);
            
            JSONParser parser = new JSONParser();
            //jsonResp = new JSONObject();
                        
            JSONArray jsonResp = (JSONArray) parser.parse(dataJson);
          

            for(int i=0; i<jsonResp.size();i++){
                Prestamo pre= new Prestamo();
                JSONObject jm = new JSONObject ();
                jm = (JSONObject) jsonResp.get(i);
                
                int id = Integer.parseInt(jm.get("idPrestamo").toString());
                int empleado = Integer.parseInt(jm.get("empleado").toString());
                int libro = Integer.parseInt(jm.get("libro").toString()); 
                int usuario = Integer.parseInt(jm.get("usuario").toString());
                
                 String fecha = jm.get("fechaSalida").toString();
                 String estado = jm.get("estado").toString();
      
     // DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
      //Date date= (Date) dateFormat.parse(fecha);
       DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
       java.util.Date convertido = fechaHora.parse(fecha);
                System.out.println(convertido);
                
                SimpleDateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                String format = writeFormat.format(convertido);
                
                //Date a = (Date) writeFormat.parse(format);
                System.out.println(format);
              pre.setIdPrestamo(id);
              pre.getEmpleado().setIdEmpleado(empleado);
              pre.getLibro().setIdLibro(libro);
              pre.getUsuario().setIdUsuario(usuario);
              pre.setFechaSalida(convertido);
              pre.setEstado(estado);
              p.add(pre);
            }
          
                         
        }catch (IOException ex) {
            System.out.println("Error en: "+ex);
        } 
        
        
        
        return p;
        
    }
    public ArrayList<Categoria> ConsumirListaCategorias() throws ParseException, MalformedURLException, IOException, org.json.simple.parser.ParseException{
        
         
        String responseString = "";
        String outputString = "";
        String wsURL = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/categorias";
          ArrayList<Categoria> cat = new ArrayList();
        try {
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection)connection;

            if(httpConn.getResponseCode()!=200){
                System.out.println("Parece que no me conecte");
                throw new RuntimeException("No se pudo conectar "+httpConn.getInputStream());
            }

            InputStreamReader isr = null;
            isr = new InputStreamReader(httpConn.getInputStream());
            BufferedReader in = new BufferedReader(isr);

            while ((responseString = in.readLine()) != null) {
            outputString = outputString + responseString;        
            }
            
            byte[] data = org.apache.commons.net.util.Base64.decodeBase64(outputString.getBytes());
            String dataJson = new String(data);
            
            JSONParser parser = new JSONParser();
            //jsonResp = new JSONObject();
                        
            JSONArray jsonResp = (JSONArray) parser.parse(dataJson);
          

            for(int i=0; i<jsonResp.size();i++){
                Categoria categoria = new Categoria();
                JSONObject jm = new JSONObject ();
                jm = (JSONObject) jsonResp.get(i);
                
                int nu = Integer.parseInt(jm.get("idCategoria").toString());
                String nombre = jm.get("categoria").toString();
                
               categoria.setIdCategoria(nu);
               categoria.setCategoria(nombre);
               cat.add(categoria);
            }
          
                         
        }catch (IOException ex) {
            System.out.println("Error en: "+ex);
        } 
        
        
        
        return cat;
        
    }  
     public ArrayList<Libro> ConsumirListaLibros() throws ParseException, MalformedURLException, IOException, org.json.simple.parser.ParseException{
          BASE64Decoder decoder = new BASE64Decoder();
         
        String responseString = "";
        String outputString = "";
        String wsURL = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/libros";
          ArrayList<Libro> lib = new ArrayList();
        try {
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection)connection;

            if(httpConn.getResponseCode()!=200){
                System.out.println("Parece que no me conecte");
                throw new RuntimeException("No se pudo conectar "+httpConn.getInputStream());
            }

            InputStreamReader isr = null;
            isr = new InputStreamReader(httpConn.getInputStream());
            BufferedReader in = new BufferedReader(isr);

            while ((responseString = in.readLine()) != null) {
            outputString = outputString + responseString;        
            }
            
            byte[] data = org.apache.commons.net.util.Base64.decodeBase64(outputString.getBytes());
            String dataJson = new String(data);
            
            JSONParser parser = new JSONParser();
            //jsonResp = new JSONObject();
                        
            JSONArray jsonResp = (JSONArray) parser.parse(dataJson);
          

            for(int i=0; i<jsonResp.size();i++){
                Libro libro = new Libro();
                JSONObject jm = new JSONObject ();
                jm = (JSONObject) jsonResp.get(i);
                
                
                int nu = Integer.parseInt(jm.get("idLibro").toString());
                String titulo = jm.get("titulo").toString();
                int idcategoria = Integer.parseInt(jm.get("categoria").toString());
                 int idautor= Integer.parseInt(jm.get("autor").toString());
              int anioEdicion= Integer.parseInt(jm.get("anio").toString());
               int cantidad = Integer.parseInt(jm.get("cantidad").toString());
              
               String portada = jm.get("portada").toString();
                byte[] imgBytes = decoder.decodeBuffer(portada);      
       
               
                libro.setIdLibro(nu);
                libro.setTitulo(titulo);
                libro.getCategoria().setIdCategoria(idcategoria);
                libro.getAutor().setIdAutor(idautor);
                libro.setAnioEdicion(anioEdicion);
                libro.setCantidd(cantidad);
                libro.setPortada(imgBytes);
                lib.add(libro);
                
                
            }
          
                         
        }catch (IOException ex) {
            System.out.println("Error en: "+ex);
        } 
        
        
        
        return lib;
        
    }  
    
    public ArrayList<Empleado> ConsumirListaEmpleados() throws ParseException, MalformedURLException, IOException, org.json.simple.parser.ParseException{
        
         
        String responseString = "";
        String outputString = "";
        String wsURL = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/empleados";
          ArrayList<Empleado> emp = new ArrayList();
        try {
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection)connection;

            if(httpConn.getResponseCode()!=200){
                System.out.println("Parece que no me conecte");
                throw new RuntimeException("No se pudo conectar "+httpConn.getInputStream());
            }

            InputStreamReader isr = null;
            isr = new InputStreamReader(httpConn.getInputStream());
            BufferedReader in = new BufferedReader(isr);

            while ((responseString = in.readLine()) != null) {
            outputString = outputString + responseString;        
            }
            
            byte[] data = org.apache.commons.net.util.Base64.decodeBase64(outputString.getBytes());
            String dataJson = new String(data);
            
            JSONParser parser = new JSONParser();
            //jsonResp = new JSONObject();
                        
            JSONArray jsonResp = (JSONArray) parser.parse(dataJson);
          

            for(int i=0; i<jsonResp.size();i++){
               Empleado em = new Empleado();
                JSONObject jm = new JSONObject ();
                jm = (JSONObject) jsonResp.get(i);
                
                
                int nu = Integer.parseInt(jm.get("idEmpleado").toString());
                String nombre = jm.get("nombre").toString();
                String apellido= jm.get("apellido").toString();
                String direccion= jm.get("direccion").toString();  
                int telefono = Integer.parseInt(jm.get("telefono").toString());
                String email= jm.get("email").toString(); 
               int puesto = Integer.parseInt(jm.get("puesto").toString());
                  em.setIdEmpleado(nu);
                  em.setNombre(nombre);
                  em.setApellido(apellido);
                  em.setDireccion(direccion);
                  em.setTelefono(telefono);
                  em.setEmail(email);
                  em.getPuesto().setIdPuesto(puesto);
                emp.add(em);
                
            }
          
                         
        }catch (IOException ex) {
            System.out.println("Error en: "+ex);
        } 
        
     return emp;
        
    }
    
    public ArrayList<Puesto> ConsumirListaPuestos() throws ParseException, MalformedURLException, IOException, org.json.simple.parser.ParseException{
        
         
        String responseString = "";
        String outputString = "";
        String wsURL = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/puestos";
          ArrayList<Puesto> pue = new ArrayList();
        try {
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection)connection;

            if(httpConn.getResponseCode()!=200){
                System.out.println("Parece que no me conecte");
                throw new RuntimeException("No se pudo conectar "+httpConn.getInputStream());
            }

            InputStreamReader isr = null;
            isr = new InputStreamReader(httpConn.getInputStream());
            BufferedReader in = new BufferedReader(isr);

            while ((responseString = in.readLine()) != null) {
            outputString = outputString + responseString;        
            }
            
            byte[] data = org.apache.commons.net.util.Base64.decodeBase64(outputString.getBytes());
            String dataJson = new String(data);
            
            JSONParser parser = new JSONParser();
            //jsonResp = new JSONObject();
                        
            JSONArray jsonResp = (JSONArray) parser.parse(dataJson);
          

            for(int i=0; i<jsonResp.size();i++){
              Puesto p = new Puesto();
                JSONObject jm = new JSONObject ();
                jm = (JSONObject) jsonResp.get(i);
                
                
                int nu = Integer.parseInt(jm.get("idPuesto").toString());
                String nombre = jm.get("puesto").toString();
                 p.setIdPuesto(nu);
                 p.setPuesto(nombre);
                 pue.add(p);
            }
          
                         
        }catch (IOException ex) {
            System.out.println("Error en: "+ex);
        } 
        
        
        
        return pue;
        
    }
    
   /* 
    public ArrayList<Prestamo> consumirListaPrestamos() throws ParseException, MalformedURLException, IOException, org.json.simple.parser.ParseException{
        
         
        String responseString = "";
        String outputString = "";
        String wsURL = "http://26.178.117.74:8081/PruebaAlternativaProyecto3/webresources/proyecres/prestamos";
          ArrayList<Prestamo> pr = new ArrayList();
        try {
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection)connection;

            if(httpConn.getResponseCode()!=200){
                System.out.println("Parece que no me conecte");
                throw new RuntimeException("No se pudo conectar "+httpConn.getInputStream());
            }

            InputStreamReader isr = null;
            isr = new InputStreamReader(httpConn.getInputStream());
            BufferedReader in = new BufferedReader(isr);

            while ((responseString = in.readLine()) != null) {
            outputString = outputString + responseString;        
            }
            
            byte[] data = org.apache.commons.net.util.Base64.decodeBase64(outputString.getBytes());
            String dataJson = new String(data);
            
            JSONParser parser = new JSONParser();
            //jsonResp = new JSONObject();
                        
            JSONArray jsonResp = (JSONArray) parser.parse(dataJson);
          

            for(int i=0; i<jsonResp.size();i++){
                Prestamo prestamo= new Prestamo();
                JSONObject jm = new JSONObject ();
                jm = (JSONObject) jsonResp.get(i);
                
                int nu = Integer.parseInt(jm.get("idPrestamo").toString());
                 int idlibro = Integer.parseInt(jm.get("libro").toString());
              int idusuario = Integer.parseInt(jm.get("usuario").toString());   
                 int idempleado = Integer.parseInt(jm.get("empleado").toString()); 
                String fecha = jm.get("fechaSalida").toString();
                  String estado = jm.get("estado").toString();
                prestamo.setIdPrestamo(nu);
                prestamo.getLibro().setIdLibro(idlibro);
                prestamo.getUsuario().setIdUsuario(idusuario);
                prestamo.setIdPrestamo(idempleado);
       
             
              Date date1=(Date) new SimpleDateFormat("dd/MM/yyyy").parse(fecha);  
                prestamo.setFechaSalida(date1);
                prestamo.setEstado(estado);
                pr.add(prestamo);
            }
          
                         
        }catch (IOException ex) {
            System.out.println("Error en: "+ex);
        } 
        
        
        
        return pr;
        
    }*/
     
    public ArrayList<Usuario> ConsumirListaUsuario() throws ParseException, MalformedURLException, IOException, org.json.simple.parser.ParseException{
        
         
        String responseString = "";
        String outputString = "";
        String wsURL = "http://10.147.20.69:8081/PruebaAlternativaProyecto3/webresources/proyecres/usuarios";
          ArrayList<Usuario> usu = new ArrayList();
        try {
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection)connection;

            if(httpConn.getResponseCode()!=200){
                System.out.println("Parece que no me conecte");
                throw new RuntimeException("No se pudo conectar "+httpConn.getInputStream());
            }

            InputStreamReader isr = null;
            isr = new InputStreamReader(httpConn.getInputStream());
            BufferedReader in = new BufferedReader(isr);

            while ((responseString = in.readLine()) != null) {
            outputString = outputString + responseString;        
            }
            
            byte[] data = org.apache.commons.net.util.Base64.decodeBase64(outputString.getBytes());
            String dataJson = new String(data);
            
            JSONParser parser = new JSONParser();
            //jsonResp = new JSONObject();
                        
            JSONArray jsonResp = (JSONArray) parser.parse(dataJson);
          

            for(int i=0; i<jsonResp.size();i++){
               Usuario  usuario = new Usuario();
                JSONObject jm = new JSONObject ();
                jm = (JSONObject) jsonResp.get(i);
             
                int nu = Integer.parseInt(jm.get("idUsuario").toString());
                String nombre = jm.get("nombre").toString();
                String apellido= jm.get("apellido").toString();
                int telefono = Integer.parseInt(jm.get("telefono").toString());
              
                String estado= jm.get("estado").toString();
                 usuario.setIdUsuario(nu);
                 usuario.setNombre(nombre);
                  usuario.setApellido(apellido);
                  usuario.setTelefono(telefono);
                usuario.setEstado(estado);
                  usu.add(usuario);

                          
            }
                        
        }catch (IOException ex) {
            System.out.println("Error en: "+ex);
        } 
         
        return usu;
        
    }
    
    
}
